package Transformer;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

/**
 * A helper class for basic geometric transformations.
 */
public interface ITransformer {

	/**
	 * Applies a 3x3 affine transformation to a set of 2D points.
	 * @param matrix The 3x3 affine transformation matrix.
	 * @param points The 3D points.
	 * @return The transformed 2D points.
	 */
	Point2d[] transform(Matrix3d matrix, Point2d... points);

	/**
	 * Converts a set of 3D points to 2D points.
	 * @param points The 3D points.
	 * @return Converted 2D points.
	 */
	Point2d[] to2D(Point3d... points);

	/**
	 * Applies a 4x4 affine transformation to a set of 3D points.
	 * @param matrix The 4x4 affine transformation matrix.
	 * @param points The 3D points.
	 * @return The transformed 3D points.
	 */
	Point3d[] transform(Matrix4d matrix, Point3d... points);

}
