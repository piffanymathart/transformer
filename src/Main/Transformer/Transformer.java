package Transformer;

import javax.vecmath.*;

/**
 * A helper class for basic geometric transformations.
 */
public class Transformer implements ITransformer {

	/**
	 * Applies a 3x3 affine transformation to a set of 2D points.
	 * @param matrix The 3x3 affine transformation matrix.
	 * @param points The 3D points.
	 * @return The transformed 2D points.
	 */
	public Point2d[] transform(Matrix3d matrix, Point2d... points) {
		int n = points.length;
		Point2d[] newPoints = new Point2d[n];
		for(int i=0; i<n; i++) {
			Point2d p = points[i];
			Point3d q = new Point3d(p.x, p.y, 1);
			matrix.transform(q);
			newPoints[i] = new Point2d(q.x, q.y);
		}
		return newPoints;
	}

	/**
	 * Converts a set of 3D points to 2D points.
	 * @param points The 3D points.
	 * @return Converted 2D points.
	 */
	public Point2d[] to2D(Point3d... points) {
		int n = points.length;
		Point2d[] newPoints = new Point2d[n];
		for(int i=0; i<n; i++) {
			Point3d p = points[i];
			newPoints[i] = new Point2d(p.x, p.y);
		}
		return newPoints;
	}

	/**
	 * Applies a 4x4 affine transformation to a set of 3D points.
	 * @param matrix The 4x4 affine transformation matrix.
	 * @param points The 3D points.
	 * @return The transformed 3D points.
	 */
	public Point3d[] transform(Matrix4d matrix, Point3d... points) {
		int n = points.length;
		Point3d[] newPoints = new Point3d[n];
		for(int i=0; i<n; i++) {
			Point3d p = points[i];
			Point4d q = new Point4d(p.x, p.y, p.z, 1);
			matrix.transform(q);
			newPoints[i] = new Point3d(q.x, q.y, q.z);
		}
		return newPoints;
	}

}
