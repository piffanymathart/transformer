package Transformer;

import org.junit.jupiter.api.Test;

import javax.vecmath.*;

import static org.junit.jupiter.api.Assertions.*;

class TransformerTest {

	private final Matrix3d MAT3x3 = new Matrix3d(
		2.0, 4.0, 8.0,
		4.0, 3.0, 3.0,
		2.0, 5.0, 4.0
	);

	private final Matrix4d MAT4x4 = new Matrix4d(
		2.0, 4.0, 8.0, 5.0,
		4.0, 3.0, 3.0, 2.0,
		2.0, 5.0, 4.0, 3.0,
		3.0, 3.0, 5.0, 2.0
	);

	@Test
	void transform_2D() {

		Point2d[] points = {
			new Point2d(11,12),
			new Point2d(21,22),
			new Point2d(31,32)
		};
		Point2d[] newPoints = new Transformer().transform(MAT3x3, points);

		for(int i=0; i<points.length; i++) {
			Point2d p = points[i];
			Point3d q = new Point3d(p.x, p.y, 1);
			MAT3x3.transform(q);
			assertEquals(newPoints[i], new Point2d(q.x, q.y));
		}
	}

	@Test
	void transform_3D() {

		Point3d[] points = {
			new Point3d(11,12,13),
			new Point3d(21,22,23),
			new Point3d(31,32,33),
			new Point3d(41,42,43)
		};
		Point3d[] newPoints = new Transformer().transform(MAT4x4, points);

		for(int i=0; i<points.length; i++) {
			Point3d p = points[i];
			Point4d q = new Point4d(p.x, p.y, p.z, 1);
			MAT4x4.transform(q);
			assertEquals(newPoints[i], new Point3d(q.x, q.y, q.z));
		}
	}

	@Test
	void to2D() {

		Point3d[] points = {
			new Point3d(11, 12, 13),
			new Point3d(21, 22, 23),
			new Point3d(31, 32, 33),
			new Point3d(41, 42, 43)
		};
		Point2d[] newPoints = new Transformer().to2D(points);

		Point2d[] expected = {
			new Point2d(11, 12),
			new Point2d(21, 22),
			new Point2d(31, 32),
			new Point2d(41, 42)
		};
		assertArrayEquals(expected, newPoints);
	}
}